package letsquehost.digikom.com.activity;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import letsquehost.digikom.com.R;
import letsquehost.digikom.com.adapter.CustomRecyclerViewAdapter;
import letsquehost.digikom.com.customview.CustomCircleView;
import letsquehost.digikom.com.room.db.DbClient;
import letsquehost.digikom.com.room.entity.Customer;
import letsquehost.digikom.com.util.HttpClient;
import letsquehost.digikom.com.util.LocalSharedPrefence;
import letsquehost.digikom.com.util.MyLifecycleHandler;
import letsquehost.digikom.com.util.OnStatusClicked;
import letsquehost.digikom.com.util.Status;
import letsquehost.digikom.com.util.interf.StatusReponse;
import letsquehost.digikom.com.util.receiver.AddCustomerResponseReceiver;

public class QueListActivity extends AppCompatActivity implements View.OnClickListener,
        PopupMenu.OnMenuItemClickListener, OnStatusClicked, StatusReponse {

    public static final String EXTRA_QUE_KEY = "com.que.key";
    public static final String ADD_CUSTOMER_RESPONSE_ACTION = "com.adc.action";
    public static final String ADD_CUSTOMER_RESPONSE_EXTRA = "com.adc.xtra";

    private RecyclerView mQueList;
    private CardView mHeader;
    private RecyclerView.LayoutManager layoutManager;
    private CustomRecyclerViewAdapter mQueAdapter;

    private TextView txtName;
    private TextView txtPhone;
    private TextView txtNumber;
    private TextView txtCheckin;
    private TextView txtPack;
    private TextView txtQueTotal;
    private ImageView imgEditHeader;
    private ImageView imgSyncStatus;
    private ImageView imgSetting;
    private LinearLayout layoutContainer;
    private FloatingActionButton mFabInput;
    private Customer mCustomerHeader;
    private CustomCircleView netState;
    private SearchView searchView;

    private List<Customer> mCustomers = new ArrayList<>();
    private List<Customer> slicedCustomers = new ArrayList<>();
    private DbClient dbClient;
    private NetworkchangeReciever mNetworkChangeReceiver;
    private AddCustomerResponseReceiver addCustomerResponseReceiver;
    private int queTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_que_detail_2);

        dbClient = DbClient.getInstance(this);

        Toolbar mToolbar = findViewById(R.id.que_list_toolbar);
        setSupportActionBar(mToolbar);

        LocalSharedPrefence.getInstance(this).setRelaunch(true);

        layoutContainer = findViewById(R.id.container_list);
        txtName = findViewById(R.id.text_name_header);
        txtNumber = findViewById(R.id.text_number_header);
        txtCheckin = findViewById(R.id.text_checkin_time_header);
        txtPack = findViewById(R.id.text_count_header);
        txtPhone = findViewById(R.id.text_phone_header);
        mFabInput = findViewById(R.id.fab_inpu);
        imgEditHeader = findViewById(R.id.img_edit_header);
        imgEditHeader.setOnClickListener(this);
        imgSetting = findViewById(R.id.img_settings);
        imgSetting.setOnClickListener(v -> startActivity(new Intent(QueListActivity.this, SettingsActivity2.class)));
        mFabInput.setOnClickListener(this);
        txtQueTotal = findViewById(R.id.text_que_value);
        netState = findViewById(R.id.view_net_stat);
        imgSyncStatus = findViewById(R.id.img_header_sync_status);
        initSearchView();

        mHeader = findViewById(R.id.card_header);
        mQueList = findViewById(R.id.recycler_que);
        mQueList.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        mQueList.setLayoutManager(layoutManager);

        mQueAdapter = new CustomRecyclerViewAdapter(mCustomers, this);
        mQueList.setAdapter(mQueAdapter);

        mNetworkChangeReceiver = new NetworkchangeReciever();
        addCustomerResponseReceiver = new AddCustomerResponseReceiver(this);

        registerReceiver(mNetworkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        //registerReceiver(addCustomerResponseReceiver, new IntentFilter(ADD_CUSTOMER_RESPONSE_ACTION));
        LocalBroadcastManager.getInstance(this).registerReceiver(addCustomerResponseReceiver, new IntentFilter(ADD_CUSTOMER_RESPONSE_ACTION));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mNetworkChangeReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNetworkChangeReceiver);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mCustomers.clear();
        mCustomerHeader = null;
        new LoadQueTask().execute();
    }

    @Override
    protected void onStop() {
        super.onStop();
        MyLifecycleHandler.relaunchAppIfNotVisible(this);
    }

    private void initList(List<Customer> customers) {
        if (customers.isEmpty()) {
            queTotal = 0;
            txtQueTotal.setText("0");
            mHeader.setVisibility(View.INVISIBLE);
            mQueList.setVisibility(View.INVISIBLE);
        } else {
            queTotal = customers.size();
            txtQueTotal.setText("" + queTotal);
            setHeader(customers.get(0));
            setQueList(customers);
        }
    }

    private void setQueList(List<Customer> customers) {
            slicedCustomers.clear();
            slicedCustomers.addAll(customers);
            slicedCustomers.remove(0);

            mHeader.setVisibility(View.VISIBLE);
            mQueList.setVisibility(View.VISIBLE);
            mCustomers.clear();
            mCustomers.addAll(slicedCustomers);
            mQueAdapter.notifyDataSetChanged();
    }

    private void setHeader(Customer customer) {
        this.mCustomerHeader = customer;
        txtPhone.setText(customer.phone);
        txtPack.setText(CustomRecyclerViewAdapter.getPack(customer.pack));
        txtName.setText(customer.name);
        txtNumber.setText("1");
        txtCheckin.setText(CustomRecyclerViewAdapter.getCheckin(customer.checkin));
        if (mCustomerHeader.synced) {
            imgSyncStatus.setImageResource(R.drawable.ic_server_sync);
        }
    }

    private void setupPopUpMenuHeader() {
        PopupMenu popupMenu = new PopupMenu(this, imgEditHeader);
        getMenuInflater().inflate(R.menu.edit_menu, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(this);
        popupMenu.show();
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == mFabInput.getId()) {
            goToInputActivity();
        } else if (v.getId() == imgEditHeader.getId()) {
            setupPopUpMenuHeader();
        }
    }

    private void goToInputActivity() {
        Intent intent = new Intent(this, QueDetailActivity.class);
        intent.putExtra(EXTRA_QUE_KEY, queTotal + 1);
        startActivity(intent);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_show:
                update(LocalSharedPrefence.getInstance(QueListActivity.this).getBranchNo(), mCustomerHeader, Status.SHOW);
                return true;
            case R.id.menu_cancel:
                update(LocalSharedPrefence.getInstance(QueListActivity.this).getBranchNo(), mCustomerHeader, Status.CANCEL);
                return true;
            case R.id.menu_no_show:
                update(LocalSharedPrefence.getInstance(QueListActivity.this).getBranchNo(), mCustomerHeader, Status.NO_SHOW);
                return true;
            default:
                return false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        layoutContainer.requestFocus();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Toast.makeText(this, query, Toast.LENGTH_SHORT).show();
        } else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            Uri uri = intent.getData();
            assert uri != null;
            String uniqueId = uri.getLastPathSegment();
            new Thread(()->{
                Customer customer = dbClient.getDb().customerDao().getCustomerByUniqueId(uniqueId);
                assert customer != null;
                runOnUiThread(()-> showStatusUpdateDialog(customer));
            }).start();
        }
    }

    private void showStatusUpdateDialog(Customer customer) {
        String title = "Update status antrian " + customer.name + "?";
        ArrayAdapter<Status> adapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_singlechoice);
        adapter.add(Status.SHOW);
        adapter.add(Status.NO_SHOW);
        adapter.add(Status.CANCEL);
        AlertDialog builder = new AlertDialog.Builder(this)
                .setTitle(title)
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setAdapter(adapter, (dialog, which) -> {
                    Status status  = adapter.getItem(which);
                    update(customer.branchNo, customer, status);
                }).create();
        builder.show();
    }

    private void initSearchView() {
        searchView = findViewById(R.id.input_search);
        SearchManager searchManager = (SearchManager) this.getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);
    }

    @Override
    public void update(String branchNo, Customer customer, Status status) {
        new Thread(() -> {
            dbClient.getDb().customerDao().updateCustomerStatusById(status.name(), customer.uid);
            HttpClient.updateCustomerStatus(status, customer.phone, branchNo, customer.uniqueId, QueListActivity.this);
            runOnUiThread(() -> new LoadQueTask().execute());
        }).start();
    }

    @Override
    public void onSyncResponseSucceed(String uid) {
        Log.d(QueListActivity.class.getSimpleName(), "Response succeed from receiver: " + uid);
        new Thread(() -> {
            dbClient.getDb().customerDao().dataSynced(uid);
            runOnUiThread(() -> new LoadQueTask().execute());
        }).start();
    }

    @Override
    public void onSyncResponseFailed(String uid) {
        Log.e(QueListActivity.class.getSimpleName(), "Response failed from receiver: " + uid);
    }

     class LoadQueTask extends AsyncTask<Void, Void, List<Customer>> {

        @Override
        protected List<Customer> doInBackground(Void... voids) {
            return dbClient.getDb().customerDao().getAllCustomers();
        }

        @Override
        protected void onPostExecute(List<Customer> customers) {
            super.onPostExecute(customers);
            initList(customers);
        }
    }

     class NetworkchangeReciever extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (isConnectedToInternet()) {
                netState.setCircleColor(Color.GREEN);
            } else {
                netState.setCircleColor(Color.RED);
            }
        }

        private boolean isConnectedToInternet() {
            ConnectivityManager cm = (ConnectivityManager) QueListActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
            return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
        }
    }
}
