package letsquehost.digikom.com.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;

import letsquehost.digikom.com.R;
import letsquehost.digikom.com.room.service.CustomerService;
import letsquehost.digikom.com.util.LocalSharedPrefence;
import letsquehost.digikom.com.util.MyLifecycleHandler;
import letsquehost.digikom.com.util.PhoneUtil;

public class QueDetailActivity extends AppCompatActivity implements View.OnClickListener{
    private AppCompatSpinner mSpinnerPax;

    private FloatingActionButton mFabSave;
    private AppCompatEditText mName;
    private AppCompatEditText mPhone;
    private CustomerService customerService;
    private int que = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_que_detail);

        Toolbar toolbar = findViewById(R.id.que_detail_toolbar);

        if (getSupportActionBar() == null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        LocalSharedPrefence.getInstance(this).setRelaunch(true);

        if (getIntent() != null) {
            que = getIntent().getIntExtra(QueListActivity.EXTRA_QUE_KEY, 1);
        }

        mFabSave = findViewById(R.id.fab_save);
        mName = findViewById(R.id.input_nama);
        mPhone = findViewById(R.id.input_phone);
        SwitchCompat mSwitchSmoke = findViewById(R.id.switch_smoke);
        mSwitchSmoke.setVisibility(View.INVISIBLE);
        mSpinnerPax = findViewById(R.id.spinner_pax);

        ArrayAdapter<CharSequence> paxAdapter = ArrayAdapter.createFromResource(this, R.array.pax_values, R.layout.custom_spinner_item);
        paxAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
        mSpinnerPax.setAdapter(paxAdapter);

        mFabSave.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        customerService = new CustomerService( this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        MyLifecycleHandler.relaunchAppIfNotVisible(this);
    }

    private boolean isInputValid() {
        int valid = 0;

        if (TextUtils.isEmpty(mName.getText().toString())) {
            valid+=1;
            mName.setError("Nama tidak boleh kosong");
        }

        if (TextUtils.isEmpty(mPhone.getText().toString())) {
            valid+=1;
            mPhone.setError("Nomor hp tidak boleh kosong");

        } else if (mPhone.getText().toString().length() < 11 || mPhone.getText().toString().length() > 12) {
            valid+=1;
            mPhone.setError("Nomor telepon tidak valid");
        }



        return valid == 0;

    }

    @Override
    public void onClick(View v) {
        if (isInputValid()){
            new AlertDialog.Builder(this)
                .setTitle(R.string.confirmation_title)
                    .setMessage(R.string.konfirmasi_customer)
                    .setPositiveButton(R.string.ok, (dialog, which) -> saveAndSync())
                    .setNegativeButton(R.string.cancel, (dialog, which) -> dialog.dismiss())
                    .create()
                    .show();
        } else {
            Snackbar.make(v, R.string.wrong_input_notif, Snackbar.LENGTH_INDEFINITE).setAction(R.string.ok, v1 -> {
            }).show();
        }
    }

    protected void saveAndSync() {
        customerService.saveCustomer(mName.getText().toString(),
                PhoneUtil.makeInternationalFormat(mPhone.getText().toString())
                , que, mSpinnerPax.getSelectedItem().toString());

        goToMain();
    }

    private void goToMain(){
        Intent intent = new Intent(this, QueListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
