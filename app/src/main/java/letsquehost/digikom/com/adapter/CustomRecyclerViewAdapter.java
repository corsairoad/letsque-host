package letsquehost.digikom.com.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import letsquehost.digikom.com.R;
import letsquehost.digikom.com.room.entity.Customer;
import letsquehost.digikom.com.util.LocalSharedPrefence;
import letsquehost.digikom.com.util.OnStatusClicked;
import letsquehost.digikom.com.util.Status;

public class CustomRecyclerViewAdapter extends RecyclerView.Adapter<CustomRecyclerViewAdapter.CustomVH> implements View.OnClickListener {

    private List<Customer> customers;
    private Context context;
    private OnStatusClicked onStatusClicked;

    public CustomRecyclerViewAdapter(List<Customer> customers, Context context) {
        this.customers = customers;
        this.context = context;
        onStatusClicked = (OnStatusClicked) context;
    }

    @NonNull
    @Override
    public CustomVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layou_list_item_child, parent, false);
        return new CustomVH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomVH holder, int position) {

        Customer customer = customers.get(position);

        holder.textNumber.setText(String.valueOf(position+2));
        holder.textName.setText(customer.name);
        holder.textPack.setText(getPack(customer.pack));
        holder.textPhone.setText(customer.phone);
        holder.textCheckin.setText(getCheckin(customer.checkin));
        holder.imgEdit.setOnClickListener(this);
        holder.imgEdit.setTag(customer);

        if (customer.synced){
            holder.imgSyncedChild.setImageResource(R.drawable.ic_server_sync);
        }
    }

    @Override
    public int getItemCount() {
        return customers.size();
    }

    public static String getCheckin(Date dateCheckin){
        return new SimpleDateFormat("HH.mm", Locale.getDefault()).format(dateCheckin) + " WIB";
    }

    public static String getPack(int pack) {
        String suffix = pack > 1 ? " Packs" : " Pack";
        return pack + suffix;
    }

    // on image edit clicked
    @Override
    public void onClick(View v) {
        Customer customer = (Customer) v.getTag();
        final PopupMenu popupMenu = new PopupMenu(context, v);
        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()){
                case R.id.menu_show:
                    update(Status.SHOW, customer);
                    return true;
                case R.id.menu_cancel:
                    update(Status.CANCEL, customer);
                    return true;
                case R.id.menu_no_show:
                    update(Status.NO_SHOW, customer);
                    return true;
                default:
                    return false;
            }
        });

        MenuInflater menuInflater = popupMenu.getMenuInflater();
        menuInflater.inflate(R.menu.edit_menu, popupMenu.getMenu());

        popupMenu.show();
    }

    private void update(Status status, Customer customer) {
        String branchNo = LocalSharedPrefence.getInstance(context).getBranchNo();
        onStatusClicked.update(branchNo, customer, status);
    }

    public static class CustomVH extends RecyclerView.ViewHolder {

        public TextView textNumber;
        public TextView textName;
        public TextView textPhone;
        public TextView textCheckin;
        public TextView textPack;
        public ImageView imgEdit;
        public ImageView imgSyncedChild;

        public CustomVH(View itemView) {
            super(itemView);
            textName = itemView.findViewById(R.id.text_name_header);
            textNumber = itemView.findViewById(R.id.text_number_header);
            textPhone = itemView.findViewById(R.id.text_phone_header);
            textCheckin = itemView.findViewById(R.id.text_checkin_time_child);
            textPack = itemView.findViewById(R.id.text_count_header);
            imgEdit = itemView.findViewById(R.id.img_edit_child);
            imgSyncedChild = itemView.findViewById(R.id.img_child_sync_status);
        }
    }
}
