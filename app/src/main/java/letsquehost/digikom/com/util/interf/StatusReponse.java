package letsquehost.digikom.com.util.interf;

public interface StatusReponse {

    void onSyncResponseSucceed(String uid);
    void onSyncResponseFailed(String uid);
}
