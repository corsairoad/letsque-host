package letsquehost.digikom.com.util;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.exception.ApolloException;
import com.google.gson.Gson;

import java.io.IOException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import letsquehost.digikom.com.activity.QueListActivity;
import letsquehost.digikom.com.model.AccessToken;
import letsquehost.digikom.com.room.entity.Customer;
import letsquehost.digikom.com.util.receiver.AddCustomerResponseReceiver;
import letsquehost.digikom.com.util.service.ReupdateJobService;
import okhttp3.Authenticator;
import okhttp3.Credentials;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.Route;

public class HttpClient {

    // private static final String AUTHENTICATION_ENDPOINT = "http://206.189.119.11:8081/v1/users/oauth/token";
    // private static final String GRAPHQL_ENDPOINT = "http://206.189.119.11:8083/v1/q/graphql";

    private static final String AUTHENTICATION_ENDPOINT = "http://192.168.40.238:8081/v1/users/oauth/token";
    private static final String GRAPHQL_ENDPOINT = "http://192.168.40.238:8083/v1/q/graphql";
    private static final String CLIENT_ID = "host_letsque";
    private static final String CLIENT_SECRET = "letsque12315";

    private static OkHttpClient.Builder clientBuilder;
    private static OkHttpClient.Builder queueingClient;

    private static void createAuthenticationHeader() {

        if (clientBuilder == null) {
            clientBuilder = new OkHttpClient.Builder();
        }

        clientBuilder.authenticator(new Authenticator() {
                    @Nullable
                    @Override
                    public Request authenticate(Route route, Response response) throws IOException {
                        String basicAuth = Credentials.basic(CLIENT_ID, CLIENT_SECRET);
                        return response.request().newBuilder()
                                .addHeader("Authorization", basicAuth)
                                .build();
                    }
                });
    }

    public static AccessToken authenticate(String username, String password, String grantType, Context context) throws IOException {
        createAuthenticationHeader();

        RequestBody requestBody = new FormBody.Builder()
                .addEncoded("username", username)
                .addEncoded("password", password)
                .addEncoded("grant_type", grantType)
                .build();

        Request request = new Request
                .Builder()
                .url(AUTHENTICATION_ENDPOINT)
                .post(requestBody).build();

        Response response = clientBuilder.build().newCall(request).execute();

        if (!response.isSuccessful()) {
            return null;
        }

        AccessToken accessToken = convertJson(response.body().string());

        saveAccessToken(accessToken, context);

        return accessToken;
    }

    private static AccessToken convertJson(String json) {
        if (json == null) {
            return null;
        }
        return new Gson().fromJson(json, AccessToken.class);
    }

    private static void saveAccessToken(AccessToken accessToken, Context context) {
        if (accessToken != null) {
            LocalSharedPrefence.getInstance(context).saveAccessTokenFromObject(accessToken);
            LocalSharedPrefence.getInstance(context).setLogin(true);
        }
    }

    public static ApolloClient setupApollo(Context context) {
        if (queueingClient == null) {
            queueingClient = new OkHttpClient.Builder()
                    .addInterceptor(chain -> {
                        Request original = chain.request();
                        Request.Builder builder = original.newBuilder().method(original.method(), original.body());
                        builder.addHeader("Authorization", "Bearer " + LocalSharedPrefence.getInstance(context).getAccessToken());
                        return chain.proceed(builder.build());
                    });
        }

        return ApolloClient.builder()
                .serverUrl(GRAPHQL_ENDPOINT)
                .okHttpClient(queueingClient.build())
                .build();
    }

    public static void addCustomer(Customer customer, Context context) {
        ApolloClient apolloClient = setupApollo(context);
        apolloClient.mutate(AddCustomerMutation
                .builder()
                .customerInput(new CustomerInput(customer.name, customer.phone, customer.email, customer.pack, customer.branchNo, customer.que, customer.uniqueId))
                .build())
                .enqueue(new ApolloCall.Callback<AddCustomerMutation.Data>() {
                    @Override
                    public void onResponse(@Nonnull com.apollographql.apollo.api.Response<AddCustomerMutation.Data> response) {
                        Log.i(HttpClient.class.getSimpleName(), "add customer succeed");

                        Intent intent = new Intent();
                        intent.setAction(QueListActivity.ADD_CUSTOMER_RESPONSE_ACTION);
                        intent.putExtra(QueListActivity.ADD_CUSTOMER_RESPONSE_EXTRA, customer.uniqueId);
                        intent.putExtra(AddCustomerResponseReceiver.FLAG, 1);

                        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

                        cancelJob(context, customer.uniqueId);
                    }

                    @Override
                    public void onFailure(@Nonnull ApolloException e) {
                        Log.i(HttpClient.class.getSimpleName(), "add customer succeed");

                        Intent intent = new Intent();
                        intent.setAction(QueListActivity.ADD_CUSTOMER_RESPONSE_ACTION);
                        intent.putExtra(QueListActivity.ADD_CUSTOMER_RESPONSE_EXTRA, customer.uniqueId);
                        intent.putExtra(AddCustomerResponseReceiver.FLAG, 0);

                        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

                        e.printStackTrace();

                        applyResyncJob(context, customer.uniqueId);
                    }
                });
    }

    private static void applyResyncJob(Context context, String uniqueId) {
        JobStreet jobStreet = JobStreet.apply(context);
        jobStreet.giveABlowJob(uniqueId);
    }

    private static void applyReupdateJob(Context context, String uniqueId, int statusId, String phoneNo, String branchNo) {
        JobStreet jobStreet = JobStreet.apply(context);
        jobStreet.giveAHandJob(uniqueId, statusId, phoneNo, branchNo);

        Log.d("Reupdate Job", "reupdate job created: " + uniqueId);
    }

    private static void cancelJob(Context context, String uniqueId) {
        JobStreet.apply(context).cancelJob(uniqueId);
    }

    public static void updateCustomerStatus(Status status, String phoneNo, String branchNo, String uniqueId, Context context) {
        ApolloClient apolloClient = setupApollo(context);
        apolloClient.mutate(UpdateCustomerMutation
        .builder()
        .branchNo(branchNo)
        .phone(phoneNo)
        .status(status)
                .uid(uniqueId)
        .build())
                .enqueue(new ApolloCall.Callback<UpdateCustomerMutation.Data>() {
                    @Override
                    public void onResponse(@Nonnull com.apollographql.apollo.api.Response<UpdateCustomerMutation.Data> response) {
                        Log.i(HttpClient.class.getSimpleName(), "Update customer succeed");
                        cancelJob(context, uniqueId.concat("uptx"));
                    }

                    @Override
                    public void onFailure(@Nonnull ApolloException e) {
                        applyReupdateJob(context, uniqueId, ReupdateJobService.getStatusId(status), phoneNo, branchNo);
                        Log.d("Reupdate Job", "apply reupdate: " + uniqueId);
                        e.printStackTrace();
                    }
                });
    }
}
