package letsquehost.digikom.com.util;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;

import letsquehost.digikom.com.util.service.ResyncJobService;
import letsquehost.digikom.com.util.service.ReupdateJobService;

public class JobStreet {

    private FirebaseJobDispatcher jobDispatcher;
    private static JobStreet jobStreet;

    private JobStreet(Context context) {
        jobDispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
    }

    public static JobStreet apply(Context context) {
        if (jobStreet == null) {
            jobStreet = new JobStreet(context);
        }

        return jobStreet;
    }

    // resync new customer
    public void giveABlowJob(String uniqueId){
        Bundle bundle = new Bundle();
        bundle.putString(ResyncJobService.UNIQUE_ID, uniqueId);

        Job job = jobDispatcher.newJobBuilder()
                .setService(ResyncJobService.class)
                .setTag(uniqueId)
                .setReplaceCurrent(true)
                .setRecurring(false)
                .setLifetime(Lifetime.FOREVER)
                .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                .setExtras(bundle)
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .setTrigger(Trigger.executionWindow(0, 30))
                .build();

        jobDispatcher.mustSchedule(job);

        Log.d(JobStreet.class.getSimpleName(), "Scheduled a job to resync: " + uniqueId);
    }

    // resync customer update
    public void giveAHandJob(String uniqueId, int statusId, String phoneNo, String branchNo){
        Bundle bundle = new Bundle();
        bundle.putString(ReupdateJobService.UNIQUE_ID_UPDATE, uniqueId);
        bundle.putInt(ReupdateJobService.STATUS_ID, statusId);
        bundle.putString(ReupdateJobService.PHONE_NO, phoneNo);
        bundle.putString(ReupdateJobService.BRANCH_NO, branchNo);

        Job job = jobDispatcher.newJobBuilder()
                .setService(ReupdateJobService.class)
                .setTag(uniqueId.concat("uptx"))
                .setReplaceCurrent(true)
                .setRecurring(false)
                .setLifetime(Lifetime.FOREVER)
                .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                .setExtras(bundle)
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .setTrigger(Trigger.executionWindow(0, 30))
                .build();

        jobDispatcher.mustSchedule(job);

        Log.d(JobStreet.class.getSimpleName(), "Scheduled a job to update: " + uniqueId);
    }

    public void cancelJob(String uniqueId) {
        jobDispatcher.cancel(uniqueId);
    }
}
