package letsquehost.digikom.com.util;

import com.apollographql.apollo.api.InputFieldMarshaller;
import com.apollographql.apollo.api.InputFieldWriter;
import com.apollographql.apollo.api.internal.Utils;

import java.io.IOException;

import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@Generated("Apollo GraphQL")
public final class CustomerInput {
  private final @Nonnull String name;

  private final @Nonnull String phoneNo;

  private final @Nullable String email;

  private final int count;

  private final @Nonnull String branchNo;

  private final int que;

  private final @Nonnull String uid;

  CustomerInput(@Nonnull String name, @Nonnull String phoneNo, @Nullable String email, int count,
      @Nonnull String branchNo, int que, @Nonnull String uid) {
    this.name = name;
    this.phoneNo = phoneNo;
    this.email = email;
    this.count = count;
    this.branchNo = branchNo;
    this.que = que;
    this.uid = uid;
  }

  public @Nonnull String name() {
    return this.name;
  }

  public @Nonnull String phoneNo() {
    return this.phoneNo;
  }

  public @Nullable String email() {
    return this.email;
  }

  public int count() {
    return this.count;
  }

  public @Nonnull String branchNo() {
    return this.branchNo;
  }

  public int que() {
    return this.que;
  }

  public @Nonnull String uid() {
    return this.uid;
  }

  public static Builder builder() {
    return new Builder();
  }

  public InputFieldMarshaller marshaller() {
    return new InputFieldMarshaller() {
      @Override
      public void marshal(InputFieldWriter writer) throws IOException {
        writer.writeString("name", name);
        writer.writeString("phoneNo", phoneNo);
        writer.writeString("email", email);
        writer.writeInt("count", count);
        writer.writeString("branchNo", branchNo);
        writer.writeInt("que", que);
        writer.writeString("uid", uid);
      }
    };
  }

  public static final class Builder {
    private @Nonnull String name;

    private @Nonnull String phoneNo;

    private @Nullable String email;

    private int count;

    private @Nonnull String branchNo;

    private int que;

    private @Nonnull String uid;

    Builder() {
    }

    public Builder name(@Nonnull String name) {
      this.name = name;
      return this;
    }

    public Builder phoneNo(@Nonnull String phoneNo) {
      this.phoneNo = phoneNo;
      return this;
    }

    public Builder email(@Nullable String email) {
      this.email = email;
      return this;
    }

    public Builder count(int count) {
      this.count = count;
      return this;
    }

    public Builder branchNo(@Nonnull String branchNo) {
      this.branchNo = branchNo;
      return this;
    }

    public Builder que(int que) {
      this.que = que;
      return this;
    }

    public Builder uid(@Nonnull String uid) {
      this.uid = uid;
      return this;
    }

    public CustomerInput build() {
      Utils.checkNotNull(name, "name == null");
      Utils.checkNotNull(phoneNo, "phoneNo == null");
      Utils.checkNotNull(branchNo, "branchNo == null");
      Utils.checkNotNull(uid, "uid == null");
      return new CustomerInput(name, phoneNo, email, count, branchNo, que, uid);
    }
  }
}
