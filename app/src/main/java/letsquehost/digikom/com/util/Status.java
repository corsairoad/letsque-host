package letsquehost.digikom.com.util;

import javax.annotation.Generated;

@Generated("Apollo GraphQL")
public enum Status {
  WAITING,

  CANCEL,

  SHOW,

  NO_SHOW
}
