package letsquehost.digikom.com.util;

public enum CustomerStatus {

    WAITING("WATING"),
    SHOW("SHOW"),
    NO_SHOW("NO_SHOW"),
    CANCEL("CANCEL");

    private String value;

    CustomerStatus(String value) {
        this.value = value;
    }
}
