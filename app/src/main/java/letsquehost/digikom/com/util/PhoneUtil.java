package letsquehost.digikom.com.util;

public class PhoneUtil {

    public static String makeInternationalFormat(String s) {
        if (s.startsWith("0")){
            return "+62".concat(s.substring(1, s.length()));
        } else {
            return "+62".concat(s);
        }
    }

}
