package letsquehost.digikom.com.util.service;

import android.util.Log;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

import letsquehost.digikom.com.room.entity.Customer;
import letsquehost.digikom.com.room.service.CustomerService;
import letsquehost.digikom.com.util.Status;

public class ReupdateJobService extends JobService {

    public static final String UNIQUE_ID_UPDATE = "unique.id.update";
    public static final String BRANCH_NO = "branch.no";
    public static final String PHONE_NO = "phone.no";
    public static final String STATUS_ID = "status.id";

    @Override
    public boolean onStartJob(JobParameters job) {


        String uniqueId = job.getExtras().getString(UNIQUE_ID_UPDATE);
        String branchNo = job.getExtras().getString(BRANCH_NO);
        String phoneNo = job.getExtras().getString(PHONE_NO);
        int statusId = job.getExtras().getInt(STATUS_ID);

        doYourUpdateBro(uniqueId, branchNo, phoneNo, statusId);

        Log.d("Reupdate Job", "Reupdate processed: " + uniqueId);

        return false;
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        return false;
    }

    private void doYourUpdateBro(String uniqueId, String branchNo, String phone, int statusId) {
        new Thread(() -> {
            CustomerService customerService = new CustomerService(this);
            Customer customer = customerService.getCustomerByUniqueId(uniqueId);
            if (customer != null) {
                customerService.update(branchNo,phone,uniqueId, getStatus(statusId));
            }
        }).start();

    }

    public static Status getStatus(int statusId) {
        switch (statusId){
            case 1:
                return Status.CANCEL;
            case 2:
                return Status.NO_SHOW;
            case 3:
                return Status.SHOW;
                default:
                    return Status.SHOW;
        }
    }

    public static int getStatusId(Status status) {
        switch (status){
            case CANCEL:
                return 1;
            case NO_SHOW:
                return 2;
            case SHOW:
                return 3;
                default:return 3;
        }
    }
}
