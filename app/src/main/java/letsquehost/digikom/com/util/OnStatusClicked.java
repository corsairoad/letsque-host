package letsquehost.digikom.com.util;

import letsquehost.digikom.com.room.entity.Customer;

public interface OnStatusClicked {

    void update(String branchNo, Customer customer, Status status);
}
