package letsquehost.digikom.com.util.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import letsquehost.digikom.com.activity.QueListActivity;
import letsquehost.digikom.com.util.interf.StatusReponse;

public class AddCustomerResponseReceiver extends BroadcastReceiver {

    private StatusReponse statusReponse;
    public static final String FLAG = "is.succeed";

    public AddCustomerResponseReceiver(Context context) {
        this.statusReponse = (StatusReponse) context;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        switch (intent.getIntExtra(FLAG, 0)){
            case 1:
                statusReponse.onSyncResponseSucceed(intent.getStringExtra(QueListActivity.ADD_CUSTOMER_RESPONSE_EXTRA));
                break;
                default:
                    statusReponse.onSyncResponseFailed(intent.getStringExtra(QueListActivity.ADD_CUSTOMER_RESPONSE_EXTRA));
                    break;
        }
    }
}
