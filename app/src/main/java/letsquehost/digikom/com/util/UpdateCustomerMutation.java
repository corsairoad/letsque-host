package letsquehost.digikom.com.util;

import com.apollographql.apollo.api.InputFieldMarshaller;
import com.apollographql.apollo.api.InputFieldWriter;
import com.apollographql.apollo.api.Mutation;
import com.apollographql.apollo.api.Operation;
import com.apollographql.apollo.api.OperationName;
import com.apollographql.apollo.api.ResponseField;
import com.apollographql.apollo.api.ResponseFieldMapper;
import com.apollographql.apollo.api.ResponseFieldMarshaller;
import com.apollographql.apollo.api.ResponseReader;
import com.apollographql.apollo.api.ResponseWriter;
import com.apollographql.apollo.api.internal.UnmodifiableMapBuilder;
import com.apollographql.apollo.api.internal.Utils;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@Generated("Apollo GraphQL")
public final class UpdateCustomerMutation implements Mutation<UpdateCustomerMutation.Data, UpdateCustomerMutation.Data, UpdateCustomerMutation.Variables> {
  public static final String OPERATION_DEFINITION = "mutation UpdateCustomer($branchNo: String!, $phone: String!, $status: Status!, $uid: String!) {\n"
      + "  updateCustomerStatus(status: $status, phoneNo: $phone, branchNo: $branchNo, uid: $uid) {\n"
      + "    __typename\n"
      + "    id\n"
      + "    name\n"
      + "    email\n"
      + "    phoneNo\n"
      + "    createdAt\n"
      + "    updatedAt\n"
      + "    status\n"
      + "    branchNo\n"
      + "    count\n"
      + "    uid\n"
      + "  }\n"
      + "}";

  public static final String QUERY_DOCUMENT = OPERATION_DEFINITION;

  private static final OperationName OPERATION_NAME = new OperationName() {
    @Override
    public String name() {
      return "UpdateCustomer";
    }
  };

  private final UpdateCustomerMutation.Variables variables;

  public UpdateCustomerMutation(@Nonnull String branchNo, @Nonnull String phone,
                                @Nonnull Status status, @Nonnull String uid) {
    Utils.checkNotNull(branchNo, "branchNo == null");
    Utils.checkNotNull(phone, "phone == null");
    Utils.checkNotNull(status, "status == null");
    Utils.checkNotNull(uid, "uid == null");
    variables = new UpdateCustomerMutation.Variables(branchNo, phone, status, uid);
  }

  @Override
  public String operationId() {
    return "a51a11ab041da6eb3ff5fd960840c667fe2600cfe3bb1327cd95d65d98659e5e";
  }

  @Override
  public String queryDocument() {
    return QUERY_DOCUMENT;
  }

  @Override
  public UpdateCustomerMutation.Data wrapData(UpdateCustomerMutation.Data data) {
    return data;
  }

  @Override
  public UpdateCustomerMutation.Variables variables() {
    return variables;
  }

  @Override
  public ResponseFieldMapper<UpdateCustomerMutation.Data> responseFieldMapper() {
    return new Data.Mapper();
  }

  public static Builder builder() {
    return new Builder();
  }

  @Override
  public OperationName name() {
    return OPERATION_NAME;
  }

  public static final class Builder {
    private @Nonnull String branchNo;

    private @Nonnull String phone;

    private @Nonnull
    Status status;

    private @Nonnull String uid;

    Builder() {
    }

    public Builder branchNo(@Nonnull String branchNo) {
      this.branchNo = branchNo;
      return this;
    }

    public Builder phone(@Nonnull String phone) {
      this.phone = phone;
      return this;
    }

    public Builder status(@Nonnull Status status) {
      this.status = status;
      return this;
    }

    public Builder uid(@Nonnull String uid) {
      this.uid = uid;
      return this;
    }

    public UpdateCustomerMutation build() {
      Utils.checkNotNull(branchNo, "branchNo == null");
      Utils.checkNotNull(phone, "phone == null");
      Utils.checkNotNull(status, "status == null");
      Utils.checkNotNull(uid, "uid == null");
      return new UpdateCustomerMutation(branchNo, phone, status, uid);
    }
  }

  public static final class Variables extends Operation.Variables {
    private final @Nonnull String branchNo;

    private final @Nonnull String phone;

    private final @Nonnull
    Status status;

    private final @Nonnull String uid;

    private final transient Map<String, Object> valueMap = new LinkedHashMap<>();

    Variables(@Nonnull String branchNo, @Nonnull String phone, @Nonnull Status status,
        @Nonnull String uid) {
      this.branchNo = branchNo;
      this.phone = phone;
      this.status = status;
      this.uid = uid;
      this.valueMap.put("branchNo", branchNo);
      this.valueMap.put("phone", phone);
      this.valueMap.put("status", status);
      this.valueMap.put("uid", uid);
    }

    public @Nonnull String branchNo() {
      return branchNo;
    }

    public @Nonnull String phone() {
      return phone;
    }

    public @Nonnull
    Status status() {
      return status;
    }

    public @Nonnull String uid() {
      return uid;
    }

    @Override
    public Map<String, Object> valueMap() {
      return Collections.unmodifiableMap(valueMap);
    }

    @Override
    public InputFieldMarshaller marshaller() {
      return new InputFieldMarshaller() {
        @Override
        public void marshal(InputFieldWriter writer) throws IOException {
          writer.writeString("branchNo", branchNo);
          writer.writeString("phone", phone);
          writer.writeString("status", status.name());
          writer.writeString("uid", uid);
        }
      };
    }
  }

  public static class Data implements Operation.Data {
    static final ResponseField[] $responseFields = {
      ResponseField.forObject("updateCustomerStatus", "updateCustomerStatus", new UnmodifiableMapBuilder<String, Object>(4)
        .put("uid", new UnmodifiableMapBuilder<String, Object>(2)
          .put("kind", "Variable")
          .put("variableName", "uid")
        .build())
        .put("phoneNo", new UnmodifiableMapBuilder<String, Object>(2)
          .put("kind", "Variable")
          .put("variableName", "phone")
        .build())
        .put("status", new UnmodifiableMapBuilder<String, Object>(2)
          .put("kind", "Variable")
          .put("variableName", "status")
        .build())
        .put("branchNo", new UnmodifiableMapBuilder<String, Object>(2)
          .put("kind", "Variable")
          .put("variableName", "branchNo")
        .build())
      .build(), true, Collections.<ResponseField.Condition>emptyList())
    };

    final @Nullable UpdateCustomerStatus updateCustomerStatus;

    private volatile String $toString;

    private volatile int $hashCode;

    private volatile boolean $hashCodeMemoized;

    public Data(@Nullable UpdateCustomerStatus updateCustomerStatus) {
      this.updateCustomerStatus = updateCustomerStatus;
    }

    public @Nullable UpdateCustomerStatus updateCustomerStatus() {
      return this.updateCustomerStatus;
    }

    public ResponseFieldMarshaller marshaller() {
      return new ResponseFieldMarshaller() {
        @Override
        public void marshal(ResponseWriter writer) {
          writer.writeObject($responseFields[0], updateCustomerStatus != null ? updateCustomerStatus.marshaller() : null);
        }
      };
    }

    @Override
    public String toString() {
      if ($toString == null) {
        $toString = "Data{"
          + "updateCustomerStatus=" + updateCustomerStatus
          + "}";
      }
      return $toString;
    }

    @Override
    public boolean equals(Object o) {
      if (o == this) {
        return true;
      }
      if (o instanceof Data) {
        Data that = (Data) o;
        return ((this.updateCustomerStatus == null) ? (that.updateCustomerStatus == null) : this.updateCustomerStatus.equals(that.updateCustomerStatus));
      }
      return false;
    }

    @Override
    public int hashCode() {
      if (!$hashCodeMemoized) {
        int h = 1;
        h *= 1000003;
        h ^= (updateCustomerStatus == null) ? 0 : updateCustomerStatus.hashCode();
        $hashCode = h;
        $hashCodeMemoized = true;
      }
      return $hashCode;
    }

    public static final class Mapper implements ResponseFieldMapper<Data> {
      final UpdateCustomerStatus.Mapper updateCustomerStatusFieldMapper = new UpdateCustomerStatus.Mapper();

      @Override
      public Data map(ResponseReader reader) {
        final UpdateCustomerStatus updateCustomerStatus = reader.readObject($responseFields[0], new ResponseReader.ObjectReader<UpdateCustomerStatus>() {
          @Override
          public UpdateCustomerStatus read(ResponseReader reader) {
            return updateCustomerStatusFieldMapper.map(reader);
          }
        });
        return new Data(updateCustomerStatus);
      }
    }
  }

  public static class UpdateCustomerStatus {
    static final ResponseField[] $responseFields = {
      ResponseField.forString("__typename", "__typename", null, false, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("id", "id", null, true, CustomType.ID, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("name", "name", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("email", "email", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("phoneNo", "phoneNo", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("createdAt", "createdAt", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("updatedAt", "updatedAt", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("status", "status", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("branchNo", "branchNo", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("count", "count", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("uid", "uid", null, true, Collections.<ResponseField.Condition>emptyList())
    };

    final @Nonnull String __typename;

    final @Nullable String id;

    final @Nullable String name;

    final @Nullable String email;

    final @Nullable String phoneNo;

    final @Nullable String createdAt;

    final @Nullable String updatedAt;

    final @Nullable String status;

    final @Nullable String branchNo;

    final @Nullable Integer count;

    final @Nullable String uid;

    private volatile String $toString;

    private volatile int $hashCode;

    private volatile boolean $hashCodeMemoized;

    public UpdateCustomerStatus(@Nonnull String __typename, @Nullable String id,
        @Nullable String name, @Nullable String email, @Nullable String phoneNo,
        @Nullable String createdAt, @Nullable String updatedAt, @Nullable String status,
        @Nullable String branchNo, @Nullable Integer count, @Nullable String uid) {
      this.__typename = Utils.checkNotNull(__typename, "__typename == null");
      this.id = id;
      this.name = name;
      this.email = email;
      this.phoneNo = phoneNo;
      this.createdAt = createdAt;
      this.updatedAt = updatedAt;
      this.status = status;
      this.branchNo = branchNo;
      this.count = count;
      this.uid = uid;
    }

    public @Nonnull String __typename() {
      return this.__typename;
    }

    public @Nullable String id() {
      return this.id;
    }

    public @Nullable String name() {
      return this.name;
    }

    public @Nullable String email() {
      return this.email;
    }

    public @Nullable String phoneNo() {
      return this.phoneNo;
    }

    public @Nullable String createdAt() {
      return this.createdAt;
    }

    public @Nullable String updatedAt() {
      return this.updatedAt;
    }

    public @Nullable String status() {
      return this.status;
    }

    public @Nullable String branchNo() {
      return this.branchNo;
    }

    public @Nullable Integer count() {
      return this.count;
    }

    public @Nullable String uid() {
      return this.uid;
    }

    public ResponseFieldMarshaller marshaller() {
      return new ResponseFieldMarshaller() {
        @Override
        public void marshal(ResponseWriter writer) {
          writer.writeString($responseFields[0], __typename);
          writer.writeCustom((ResponseField.CustomTypeField) $responseFields[1], id);
          writer.writeString($responseFields[2], name);
          writer.writeString($responseFields[3], email);
          writer.writeString($responseFields[4], phoneNo);
          writer.writeString($responseFields[5], createdAt);
          writer.writeString($responseFields[6], updatedAt);
          writer.writeString($responseFields[7], status);
          writer.writeString($responseFields[8], branchNo);
          writer.writeInt($responseFields[9], count);
          writer.writeString($responseFields[10], uid);
        }
      };
    }

    @Override
    public String toString() {
      if ($toString == null) {
        $toString = "UpdateCustomerStatus{"
          + "__typename=" + __typename + ", "
          + "id=" + id + ", "
          + "name=" + name + ", "
          + "email=" + email + ", "
          + "phoneNo=" + phoneNo + ", "
          + "createdAt=" + createdAt + ", "
          + "updatedAt=" + updatedAt + ", "
          + "status=" + status + ", "
          + "branchNo=" + branchNo + ", "
          + "count=" + count + ", "
          + "uid=" + uid
          + "}";
      }
      return $toString;
    }

    @Override
    public boolean equals(Object o) {
      if (o == this) {
        return true;
      }
      if (o instanceof UpdateCustomerStatus) {
        UpdateCustomerStatus that = (UpdateCustomerStatus) o;
        return this.__typename.equals(that.__typename)
         && ((this.id == null) ? (that.id == null) : this.id.equals(that.id))
         && ((this.name == null) ? (that.name == null) : this.name.equals(that.name))
         && ((this.email == null) ? (that.email == null) : this.email.equals(that.email))
         && ((this.phoneNo == null) ? (that.phoneNo == null) : this.phoneNo.equals(that.phoneNo))
         && ((this.createdAt == null) ? (that.createdAt == null) : this.createdAt.equals(that.createdAt))
         && ((this.updatedAt == null) ? (that.updatedAt == null) : this.updatedAt.equals(that.updatedAt))
         && ((this.status == null) ? (that.status == null) : this.status.equals(that.status))
         && ((this.branchNo == null) ? (that.branchNo == null) : this.branchNo.equals(that.branchNo))
         && ((this.count == null) ? (that.count == null) : this.count.equals(that.count))
         && ((this.uid == null) ? (that.uid == null) : this.uid.equals(that.uid));
      }
      return false;
    }

    @Override
    public int hashCode() {
      if (!$hashCodeMemoized) {
        int h = 1;
        h *= 1000003;
        h ^= __typename.hashCode();
        h *= 1000003;
        h ^= (id == null) ? 0 : id.hashCode();
        h *= 1000003;
        h ^= (name == null) ? 0 : name.hashCode();
        h *= 1000003;
        h ^= (email == null) ? 0 : email.hashCode();
        h *= 1000003;
        h ^= (phoneNo == null) ? 0 : phoneNo.hashCode();
        h *= 1000003;
        h ^= (createdAt == null) ? 0 : createdAt.hashCode();
        h *= 1000003;
        h ^= (updatedAt == null) ? 0 : updatedAt.hashCode();
        h *= 1000003;
        h ^= (status == null) ? 0 : status.hashCode();
        h *= 1000003;
        h ^= (branchNo == null) ? 0 : branchNo.hashCode();
        h *= 1000003;
        h ^= (count == null) ? 0 : count.hashCode();
        h *= 1000003;
        h ^= (uid == null) ? 0 : uid.hashCode();
        $hashCode = h;
        $hashCodeMemoized = true;
      }
      return $hashCode;
    }

    public static final class Mapper implements ResponseFieldMapper<UpdateCustomerStatus> {
      @Override
      public UpdateCustomerStatus map(ResponseReader reader) {
        final String __typename = reader.readString($responseFields[0]);
        final String id = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[1]);
        final String name = reader.readString($responseFields[2]);
        final String email = reader.readString($responseFields[3]);
        final String phoneNo = reader.readString($responseFields[4]);
        final String createdAt = reader.readString($responseFields[5]);
        final String updatedAt = reader.readString($responseFields[6]);
        final String status = reader.readString($responseFields[7]);
        final String branchNo = reader.readString($responseFields[8]);
        final Integer count = reader.readInt($responseFields[9]);
        final String uid = reader.readString($responseFields[10]);
        return new UpdateCustomerStatus(__typename, id, name, email, phoneNo, createdAt, updatedAt, status, branchNo, count, uid);
      }
    }
  }
}
