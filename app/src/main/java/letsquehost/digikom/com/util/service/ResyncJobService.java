package letsquehost.digikom.com.util.service;

import android.util.Log;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

import letsquehost.digikom.com.room.entity.Customer;
import letsquehost.digikom.com.room.service.CustomerService;

public class ResyncJobService extends JobService {

    public static final String UNIQUE_ID = "unique.id";
    public static final String TAG = ResyncJobService.class.getSimpleName();

    @Override
    public boolean onStartJob(JobParameters job) {
        Log.d(TAG, "Job service called");

        doYourJobBuddy(job.getExtras().getString(UNIQUE_ID));

        return false;
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        return false;
    }

    private void doYourJobBuddy(String uniqueId) {
        new Thread(()->{
            CustomerService customerService = new CustomerService(ResyncJobService.this);
            Customer customer = customerService.getCustomerByUniqueId(uniqueId);
            if (customer !=null) {
                Log.d(TAG, "Resync customer: " + uniqueId);
                customerService.send(customer);
            }
        }) .start();
    }
}
