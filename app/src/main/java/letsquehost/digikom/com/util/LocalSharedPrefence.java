package letsquehost.digikom.com.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import letsquehost.digikom.com.R;
import letsquehost.digikom.com.activity.QueListActivity;
import letsquehost.digikom.com.model.AccessToken;

public class LocalSharedPrefence {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private static LocalSharedPrefence localSharedPrefence;
    private Context context;
    public static final String KEY_RELAUNCH_APP = "relaunch";

    private LocalSharedPrefence(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(QueListActivity.class.getSimpleName(), Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public static LocalSharedPrefence getInstance(Context context) {

        if (localSharedPrefence == null) {
            localSharedPrefence = new LocalSharedPrefence(context);
        }

        return localSharedPrefence;
    }

    public void saveAccessToken(String accessToken){
        this.editor.putString(getString(R.string.token_accesstoken), accessToken)
                .commit();
    }

    public String getAccessToken(){
        return sharedPreferences.getString(getString(R.string.token_accesstoken), null);
    }

    public void saveRefreshToken(String refreshToken) {
        editor.putString(getString(R.string.token_refreshtoken), refreshToken)
                .commit();
    }

    public String getRefreshToken() {
        return sharedPreferences.getString(getString(R.string.token_refreshtoken), null);
    }

    public void saveBranchNo(String branchNo) {
        editor.putString(getString(R.string.token_branchno), branchNo)
                .commit();
    }

    public void setLogin(boolean isLogin) {
        editor.putBoolean(getString(R.string.isloggedin), isLogin)
                .commit();
    }

    public boolean isLoggedIn() {
        return sharedPreferences.getBoolean(getString(R.string.isloggedin), false);
    }

    public String getBranchNo() {
        return sharedPreferences.getString(getString(R.string.token_branchno), null);
    }

    public void saveMerchant(String merchant) {
        editor.putString(getString(R.string.token_merchant), merchant)
                .commit();
    }

    public String getMerchant() {
        return sharedPreferences.getString(getString(R.string.token_merchant), null);
    }

    private String getString(int id) {
        return context.getString(id);
    }

    public void saveAccessTokenFromObject(AccessToken accessToken) {
        saveAccessToken(accessToken.getAccessToken());
        saveRefreshToken(accessToken.getRefreshToken());
        saveBranchNo(accessToken.getBranchNo());
        saveMerchant(accessToken.getMerchant());
    }

    public void setRelaunch(boolean relaunch) {
        editor.putBoolean(KEY_RELAUNCH_APP, relaunch);
        editor.commit();
    }

    public boolean getRelaunch() {
        return sharedPreferences.getBoolean(KEY_RELAUNCH_APP, true);
    }
}
