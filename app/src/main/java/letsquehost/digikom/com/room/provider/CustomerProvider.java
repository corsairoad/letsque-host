package letsquehost.digikom.com.room.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import letsquehost.digikom.com.room.service.CustomerService;

public class CustomerProvider extends ContentProvider {

    CustomerService customerService;

    @Override
    public boolean onCreate() {
        customerService = new CustomerService(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        String query = uri.getLastPathSegment().toLowerCase();
        Cursor cursor = customerService.getCustomerDao().getFilteredCustomers("%" + query + "%");
        if (cursor.moveToFirst()) {
            while (cursor.moveToNext()) {
                Log.d(CustomerProvider.class.getSimpleName(), cursor.getString(1));
            }
        }
        Log.d(CustomerProvider.class.getSimpleName(), "Total: " + cursor.getCount());
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
