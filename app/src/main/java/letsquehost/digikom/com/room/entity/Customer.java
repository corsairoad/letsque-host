package letsquehost.digikom.com.room.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.Date;

import letsquehost.digikom.com.util.CustomerStatus;

@Entity(tableName = "customer")
public class Customer {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "_ID")
    public int uid;
    @ColumnInfo(name = "name")
    public String name;
    @ColumnInfo(name = "phone")
    public String phone;
    @ColumnInfo(name = "email")
    public String email;
    @ColumnInfo(name = "pack")
    public int pack;
    @ColumnInfo(name = "branch_no")
    public String branchNo;
    @ColumnInfo(name = "que")
    public int que;
    @ColumnInfo(name = "checkin")
    public Date checkin;
    @ColumnInfo(name = "update_time")
    public Date updateTime;
    @ColumnInfo(name = "status")
    public String status;
    @ColumnInfo(name = "synced")
    public boolean synced;
    @ColumnInfo(name = "uniqueId")
    public String uniqueId;

    public Customer() {
    }
}
