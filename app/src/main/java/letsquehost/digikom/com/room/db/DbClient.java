package letsquehost.digikom.com.room.db;

import android.arch.persistence.room.Room;
import android.content.Context;

import letsquehost.digikom.com.room.dao.CustomerDao;

public class DbClient {

    private Context context;
    private AppDatabase appDatabase;
    private static DbClient dbClient;

    private DbClient(Context context) {
        this.context = context;
        appDatabase = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "customers").build();
    }

    public static DbClient getInstance(Context context) {
        if (dbClient == null) {
            dbClient = new DbClient(context);
        }

        return dbClient;
    }


    public AppDatabase getDb() {
        return appDatabase;
    }
}
