package letsquehost.digikom.com.room.service;

import android.content.Context;

import java.util.Date;
import java.util.UUID;

import letsquehost.digikom.com.room.dao.CustomerDao;
import letsquehost.digikom.com.room.db.DbClient;
import letsquehost.digikom.com.room.entity.Customer;
import letsquehost.digikom.com.util.CustomerStatus;
import letsquehost.digikom.com.util.HttpClient;
import letsquehost.digikom.com.util.LocalSharedPrefence;
import letsquehost.digikom.com.util.Status;

public class CustomerService {

    private DbClient dbClient;
    private CustomerDao customerDao;
    private LocalSharedPrefence sharedPrefence;
    private Context context;

    public CustomerService(Context context) {
        this.dbClient = DbClient.getInstance(context);
        this.context = context;
        customerDao = dbClient.getDb().customerDao();
        sharedPrefence = LocalSharedPrefence.getInstance(context);
    }

    // save do local db
    public void saveCustomer(final String name, final String phone, final int que, final String pack) {

        new Thread(() -> {
            Customer customer = new Customer();
            customer.que = que;
            customer.pack = Integer.valueOf(pack);
            customer.name = name;
            customer.phone = phone;
            customer.status = CustomerStatus.WAITING.name();
            customer.branchNo = sharedPrefence.getBranchNo();
            customer.checkin = new Date();
            customer.uniqueId = generateUniqueId();

            customerDao.insertCustomer(customer);

            // send to server
            send(customer);
        }).start();
    }

    private String generateUniqueId() {
        return UUID.randomUUID().toString().substring(0, 9);
    }

    public void send(Customer customer) {
        HttpClient.addCustomer(customer, context);
    }

    public void update(String branchNo, String phone, String uniqueId, Status status) {
        HttpClient.updateCustomerStatus(status, phone, branchNo, uniqueId, context);
    }

    public Customer getCustomerByUniqueId(String uniqueId) {
        return customerDao.getCustomerByUniqueId(uniqueId);
    }

    public DbClient getDbClient() {
        return dbClient;
    }

    public CustomerDao getCustomerDao() {
        return customerDao;
    }

}
