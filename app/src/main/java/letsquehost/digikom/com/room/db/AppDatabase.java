package letsquehost.digikom.com.room.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import letsquehost.digikom.com.room.dao.CustomerDao;
import letsquehost.digikom.com.room.entity.Customer;

@Database(entities = {Customer.class}, version = 2)
@TypeConverters({letsquehost.digikom.com.room.TypeConverters.class})
public abstract class AppDatabase extends RoomDatabase {

    public abstract CustomerDao customerDao();

}
