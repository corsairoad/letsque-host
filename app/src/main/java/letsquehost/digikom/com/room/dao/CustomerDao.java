package letsquehost.digikom.com.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.database.Cursor;

import java.util.List;

import letsquehost.digikom.com.room.entity.Customer;

@Dao
public interface CustomerDao {

    @Query("SELECT * from customer WHERE status = 'WAITING' ORDER BY checkin ASC")
    List<Customer> getAllCustomers();

    @Insert
    Long insertCustomer(Customer customer);

    @Query("UPDATE customer set status = :status where _ID = :id")
    void updateCustomerStatusById(String status, int id);

    @Query("UPDATE customer set synced = 1 where uniqueId = :uniqueId")
    void dataSynced(String uniqueId);

    @Query("SELECT * FROM customer WHERE status = 'WAITING' and synced = 0 ORDER BY checkin ASC")
    List<Customer> getUnsyncedCustomers();

    @Query("SELECT * FROM customer WHERE uniqueId = :uniqueId")
    Customer getCustomerByUniqueId(String uniqueId);

    @Query("SELECT _ID as '_id', name as 'suggest_text_1', phone as 'suggest_text_2', uniqueId as 'suggest_intent_data_id' FROM customer WHERE status = 'WAITING' AND name LIKE :query or phone LIKE :query")
    Cursor getFilteredCustomers(String query);
}
